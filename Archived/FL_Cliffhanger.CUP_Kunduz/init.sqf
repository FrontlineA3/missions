enableSaving [false, false];
waitUntil {!isNil "CLib_fnc_loadModules"};
call CLib_fnc_loadModules;

if (isServer) then {
	[] spawn {
		sleep 3;
		{
			private _wall = nearestObject [_x, "House"];
			if ((toLower (typeOf _wall) find "wall") != -1) then {
				_wall setDamage 1;
			};
			} forEach [
			[2066, 3112, 0],
			[1825, 3288, 0],
			[1754, 3339, 0],
			[1874, 3005, 0],
			[1995, 3144, 0]
		];
	}
};
