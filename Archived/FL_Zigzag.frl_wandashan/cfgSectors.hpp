class sector {
    bleedValue[] = {10,10}; // Frontline
    captureTime[] = {120,250};
    firstCaptureTime[] = {90,180};
    sectortype = "normal";

    dependency[] = {};
    ticketValue = 0;
    minUnits = 1;
    isLastSector = "";
};

class CfgSectors {

    class base_west : sector {
        designator = "HQ";
        sectortype = "mainbase";
        bleedValue = 0;
    };

    class base_east : sector {
        designator = "HQ";
        sectortype = "mainbase";
        bleedValue = 0;
    };

    class CfgSectorPath {
        class path_0 {
            /*class sector_0 : sector {
                dependency[] = {"base_west","sector_1"};
                designator = "A";
            };*/

            class sector_1 : sector {
                dependency[] = {"base_west","sector_2"};
                designator = "A";
            };

            class sector_2 : sector {
                dependency[] = {"sector_1","sector_3"};
                designator = "B";
            };

            class sector_3 : sector {
                dependency[] = {"sector_2","sector_4"};
                designator = "C";
            };

			      class sector_4 : sector {
                dependency[] = {"sector_3","sector_5"};
                designator = "D";
            };

			      class sector_5 : sector {
                dependency[] = {"sector_4","sector_6"};
                designator = "E";
            };

			      class sector_6 : sector {
                dependency[] = {"sector_5","base_east"};
                designator = "F";
            };
        };
       
    };
};
