#!/usr/bin/env python

from __future__ import unicode_literals
from __future__ import print_function

import argparse
import os
import subprocess
import sys
import textwrap
import codecs

import common.colorsLog as color


def print_build_fail():
    color.print_red("Build failed: ", end='')


def file_readable(file_path, file_name):
    """ returns True if file exists and is readable, False otherwise """
    file_to_check = os.path.join(file_path, file_name)
    if not os.path.isfile(file_to_check):
        print_build_fail()
        print("file does not exist {}".format(file_name))
        return False
    # check if file is readable by trying to read each line, and excepting
    # with a build failure if we cannot read a line
    try:
        with codecs.open(file_to_check, 'r', encoding='utf8', errors='strict') as opened_file:
            for line in opened_file:
                pass
    except UnicodeError:
        print_build_fail()
        print("could not read file {}: is it rapified?".format(file_name))
        return False
    return True


def ensure_at_least_one(file_path, file_name, parameter, value):
    """
    Checks file_name to ensure parameter is equal to value at least once,
    and return True if it is. False by default. Only works for type(value)==int
    """
    file_to_check = os.path.join(file_path, file_name)
    if not file_readable(file_path, file_name):
        return False
    with open(file_to_check, 'r') as opened_file:
        for num, line in enumerate(opened_file, 1):
            line = line.lstrip().rstrip().rstrip(';')
            line = line.split('=')
            if line[0] == parameter:
                try:
                    parsedvalue = int(line[1])
                except ValueError:
                    print_build_fail()
                    print("{par} does not correspond to an integer on line "
                          "{line} in {path}".format(par=parameter, line=num, path=file_name))
                    return False
                if parsedvalue == value:
                    return True
    print_build_fail()
    print("{par} must equal {val} at least once in {path}".format(par=parameter, val=value, path=file_name))
    return False


def ensure_none(file_path, file_name, parameter, value):
    """
    Checks file_name to ensure parameter is never equal to value,
    and return False if it is. True by default. Only works for type(value)==int
    """
    file_to_check = os.path.join(file_path, file_name)
    if not file_readable(file_path, file_name):
        return False
    with open(file_to_check, 'r') as opened_file:
        for num, line in enumerate(opened_file, 1):
            line = line.lstrip().rstrip().rstrip(';')
            line = line.split('=')
            if line[0] == parameter:
                try:
                    parsedvalue = int(line.split('=')[1])
                except ValueError:
                    print_build_fail()
                    print("{par} does not correspond to an integer on line "
                          "{line} in {path}".format(par=parameter, line=num, path=file_name))
                    return False
                if parsedvalue == value:
                    print_build_fail()
                    print("{par} = {val} on line {line} in {path}".format(
                          par=parameter, val=parsedvalue, line=num, path=file_name))
                    return False
    return True


def build_pbo(path, output_dir, build_version):
    # Split up  mission name and terrain name. Terrain name shouldn't be
    # adjusted, so add it to back of missionname
    missionname = os.path.basename(path).split('.')
    if build_version:
        output_name = "{}_v{}.{}.pbo".format(missionname[0], build_version, missionname[len(missionname) - 1])
    else:
        output_name = "{}.{}.pbo".format(missionname[0], missionname[len(missionname) - 1])

    args = ['makepbo', '-NUP', path]
    if output_dir:
        output_file = os.path.join(output_dir, output_name)
        args.append(output_file)

    proc = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    (stdout_data, stderr_data) = proc.communicate()
    retval = proc.wait()

    if retval != 0:
        print(textwrap.dedent('''
            {delimiter}
            ERROR: {path}
            {delimiter}
            {message}
            {delimiter}
            ''').format(
                delimiter='=' * 80,
                message=stdout_data,
                path=path
        ))
    else:
        color.print_green(' OK!')
    return retval


def build_pbos(args, build_directory, output_directory):
    retval = 0
    print('Source: {}'.format(build_directory))
    print('Target: {}'.format(output_directory))
    build_version = os.environ.get('CI_JOB_ID', '1')

    for entry in os.listdir(build_directory):
        path = os.path.join(build_directory, entry)
        if not os.path.isdir(path):
            color.print_red('Skipping {} because it is not a directory'.format(entry))
            continue

        if any(entry.startswith(exclude_prefix) for exclude_prefix in args.prefix_exclude):
            color.print_red('Skipping {} because it it matches an exclude prefix'.format(entry))
            continue

        if not any(entry.startswith(include_prefix) for include_prefix in args.prefix_directory):
            color.print_red('Skipping {} because it doesn\'t match an include prefix'.format(entry))
            continue

        if not os.path.isfile(os.path.join(path, "metadata.hpp")):
            color.print_red('Skipping {} because of no metadata.hpp file present'.format(entry))
            continue

        color.print_yellow('Building ', '')
        print('{}...'.format(path), end='')

        sys.stdout.flush()
        # Check if enableDebugConsole is 2 in description.ext,
        # and fail build if so
        if not ensure_none(path, 'description.ext', 'enableDebugConsole', 2):
            retval = 1
            continue
        # Check if at least one isPlayable=1 in mission.sqm and fail otherwise
        if not ensure_at_least_one(path, 'mission.sqm', 'isPlayable', 1):
            retval = 1
            continue
        # Build the entry, if not successfully built, set retval to 1
        # to specify an unsuccessful build
        # Don't add version info to autorotations
        if build_pbo(path, output_directory, build_version if 'DFL_autorotation' not in path else None) != 0:
            retval = 1

    return retval


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Build missions and check if those missions are valid')

    parser.add_argument('directory', nargs='?', default='Frontline\RHS',
                        help='Directory containing missions directories')
    parser.add_argument('-p', '--prefix-directory', default=['DFL_', 'FL_'],
                        help='Make missions only starting with this prefix', action='append')
    parser.add_argument('-x', '--prefix-exclude', default=[],
                        help='Make missions only starting with this prefix', action='append')
    parser.add_argument('-o', '--output-directory', nargs='?', default='build\RHS',
                        help='Create pbo files in the given directory')

    args = parser.parse_args()
    base_directory = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
    build_directory = os.path.join(base_directory, args.directory)
    output_directory = os.path.join(base_directory, args.output_directory)

    sys.exit(build_pbos(args, build_directory, output_directory))
