class CfgAI {
    preferedUnitCount = 25; // How much units we want (AI - pla) 3x6 man squads
    squadCreateCooldown = 150; // Time between squad creation
    //distanceFilter = 500; // Only allow spawning AI if spawnpoint is within this distance
};
