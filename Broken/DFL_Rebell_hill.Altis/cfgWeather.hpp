class CfgWeather {
	overcastRandomize = 1;
	overcastFrame[] = {0,1}; // [0,1]

	rainRandomize = 1;
	rainFrame[] = {0,1}; // [0,1]

	fogRandomize = 1;
	fogFrame[] = {0,0.09}; // [0,1]

	timeRandomize = 1;
	timeFrame[] = {5.8,5.95}; // [0,24]
};
