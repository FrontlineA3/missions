#define TEAM_EAST       0
#define TEAM_WEST       1
#define TEAM_IND 		2
#define TEAM_CIV   		3

class FRL_squads {
	#include "Vanilla\A3_NATO_squads.hpp"
	#include "Vanilla\A3_CSAT_squads.hpp"
};

class FRL_kits {
	// -- Only include the roles you want to overwrite. These are just listed here as a reference -- //
	// #include "Vanilla\A3_NATO_squads.hpp"
	// #include "Vanilla\A3_CSAT_squads.hpp"
};
