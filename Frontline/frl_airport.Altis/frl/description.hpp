#include "kitsList.hpp"

class Header {
	gameType = SC;
	minPlayers = 2;
	maxPlayers = 100;
  	playerCountMultipleOf = 2;
};

respawn				= 3;
respawnTemplates[] 	= {};
respawnDialog		= 0;

joinUnassigned 		= 0;
disabledAI			= 1;
aiKills				= 0;

// -- Not functional for MP. Have these here so people don't add nonsense in Attributes -- //
briefing			= 0;
overviewTextLocked	= "";
keys[]				= {};
keysLimit			= 0;
doneKeys[]			= {};
