// -- Radio settings -- //
frl_radio_channel_name = "TacBF Team";
frl_radio_channel_password = "1234";
frl_radio_enforced = false; // Force usage. Requires clients to be connected to the right channel and have their plugin enabled
frl_radio_ip = "ts.frontline.frl";

frl_spawnpoints_rallyPointDuration = 30; // Time rally point stays active for after placement
frl_spawnpoints_rallyPointCooldown = 10; // Time after deactivation that you can place a new rally point
frl_spawnpoints_rallyPointCooldownDestroyed = 10; // Time after destroying that you can place a new rally point