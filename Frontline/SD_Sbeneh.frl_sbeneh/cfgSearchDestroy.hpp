class CfgSearchDestroy {
  //bleedStrength = 10; // -- Victory points for defenders per 10 minutes
  //bleedPerCache = 25;
  //intelStages[] = {10, 40, 60, 80, 100};
    markerSizes[] = {155, 200, 350, 500, 650, 800};

  //cacheSpreadMin = 20; // -- Minimum distance between caches in a single objective
  //cacheSpreadMax = 120; // -- Maximum distance between caches
  //objectiveSpacing = 300; // -- Minimum distance to the next objective set

  //passiveIntel = 10; // -- Intel automatically gained per 10 minutes
  //deathIntel = 1; // Intel per defender death

  //tunnelSize = 50;
  //tunnelTime = -1;

  //bombArmTime = 10; // Time required to arm bomb
  //bombDefuseTime = 5; // Time required to defuse bomb
  //bombCookoffTime = 30; // Time bomb needs to tick before it goes boom


  //preventBuildingDamage = 1; // -- 1 or higher to prevent building of cache taking damage
};
