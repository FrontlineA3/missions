class O_A3_CSAT_Squad_Basic1 {
	defaultSquad = 1;
	displayName = "Basic";
	description = "Infantry";
	side = TEAM_EAST;
	type = 2;
	roles[] = {
		{"O_A3_CSAT_SquadLeader"},
		{"O_A3_CSAT_Default",1},
		{"O_A3_CSAT_Medic",1},
		{"O_A3_CSAT_MG", 1},
		{"O_A3_CSAT_Default", 4},
		{"O_A3_CSAT_Grenadier", 4},
		{"O_A3_CSAT_LAT", 4},
		{"O_A3_CSAT_Engineer", 6},
		{"O_A3_CSAT_Marksman", 8}
	};
};

class O_A3_CSAT_Squad_Basic2: O_A3_CSAT_Squad_Basic1 {
	defaultSquad = 0;
	displayName = "Bravo";
	description = "Infantry";
};


class O_A3_CSAT_Squad_MG1 {
	defaultSquad = 1;
	displayName = "Charlie";
	description = "MG Support";
	side = TEAM_EAST;
	type = 2;
	roles[] = {
		{"O_A3_CSAT_TeamLeader_MG"},
		{"O_A3_CSAT_MG",1},
		{"O_A3_CSAT_MG",2}
	};
};
class O_A3_CSAT_Squad_Recon1 {
	defaultSquad = 1;
	displayName = "Delta";
	description = "Recon";
	side = TEAM_EAST;
	type = 2;
	roles[] = {
		{"O_A3_CSAT_TeamLeader_Recon"},
		{"O_A3_CSAT_Rifleman_Recon",1},
		{"O_A3_CSAT_Medic_Recon",1},
		{"O_A3_CSAT_Rifleman_Recon", 3},
		{"O_A3_CSAT_Engineer_Recon", 3},
		{"O_A3_CSAT_LAT_Recon", 5},
	};
};

class O_A3_CSAT_Squad_Sniper1 {
	defaultSquad = 1;
	displayName = "Noobs";
	description = "Sniper Team";
	side = TEAM_EAST;
	type = 2;
	roles[] = {
		{"O_A3_CSAT_Spotter"},
		{"O_A3_CSAT_Sniper",1}
	};
};

