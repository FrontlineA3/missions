class CfgSectors {
    class base_west {
        designator = "HQ"; // Name in spawn list
        sectortype = "mainbase"; // Mainbase to allow spawning
        aiSpawnAllow = 1; // 0 to prevent AI spawning here
        aiSpawnOnly = 0; // 1 to only allow AI spawning here
        spawnMarker = "baseSpawn_west"; // Different marker than  middle
        //spawnHeight = 15; // Height above sea/terrain
    };
    
   class base_west_1 {
        designator = "Airfield";
        sectortype = "mainbase";
        spawnMarker = "baseSpawn_west_1";
        aiSpawnAllow = 1;
        aiSpawnOnly = 0;
    };
    
    class base_east {
        designator = "HQ";
        sectortype = "mainbase";
        aiSpawnAllow = 0;
        spawnMarker = "baseSpawn_east";
    };
    
   class base_east_1 {
        designator = "FB West";
        sectortype = "mainbase";
        spawnMarker = "baseSpawn_east_1";
        aiSpawnAllow = 0;
        aiSpawnOnly = 0;
    };
    
   class base_east_2 {
        designator = "FB East";
        sectortype = "mainbase";
        spawnMarker = "baseSpawn_east_2";
        aiSpawnAllow = 0;
        aiSpawnOnly = 0;
    };
};
