class CfgSectors {
    class base_west {
        designator = "HQ";
        sectortype = "mainbase";
        aiSpawnAllow = 0;
    };
	class base_west_1 {
        designator = "";
        sectortype = "mainbase";
        aiSpawnAllow = 1;
        aiSpawnOnly = 1;
    };

    class base_east {
        designator = "HQ";
        sectortype = "mainbase";
        aiSpawnAllow = 0;
    };
	
	class base_east_1 {
        designator = "";
        sectortype = "mainbase";
        aiSpawnAllow = 1;
        aiSpawnOnly = 1;
    };
};
