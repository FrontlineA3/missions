class cfgFactions {
    class West {
        factionClass = "RHS_AFP";
    };

    class East : West {
        factionClass = "RHS_INS_W";
    };
};
